if (prev.getResponseCode() == "302") {
	
	/*
	Get the Location HTTP header
	*/
	headerList = prev.getResponseHeaders()
	headerList = headerList + "\n"  // simplifies substring search in case header is at the end of the string
	log.debug("***** headerList: {}", headerList)
	header = "Location: "
	
	s = headerList.substring(headerList.indexOf(header))
	location = s.substring(header.length(), s.indexOf("\n"))
	
	log.debug("***** Location: {}", location)
	
	URL url = new URL(location)
	
	port = (url.getPort() === -1) ? url.getDefaultPort() : url.getPort()
	path = url.getPath()
	query = url.getQuery()
	
	if ((null == query) || (query.isEmpty()) ) {
		path = url.getPath()
	} else {
		path = url.getPath() + "?" + query
	}
	
	vars.put("redirect-scheme", url.getProtocol())
	vars.put("redirect-host", url.getHost())
	vars.putObject("redirect-port", port)
	vars.put("redirect-path", path)
	
	log.debug("***** {}", url.getProtocol())
	log.debug("***** {}", url.getHost())
	log.debug("***** {}", port)
	log.debug("***** {}", path)

	if (path.contains("error_description")) {
		AssertionResult.setFailureMessage("***** Path contains error_description");
		AssertionResult.setFailure(true);
	}
} else {
	log.error("***** Expected 302 got {}", prev.getResponseCode())
	AssertionResult.setFailure(true);
}
