if (prev.getResponseCode() == "302") {
	
	/*
	Get the Location HTTP header
	*/
	headerList = prev.getResponseHeaders()
	headerList = headerList + "\n"  // simplifies substring search in case header is at the end of the string
	log.debug("***** headerList: {}", headerList)
	header = "Location: "
	
	s = headerList.substring(headerList.indexOf(header))
	location = s.substring(header.length(), s.indexOf("\n"))
	
	log.debug("***** Location: {}", location)
	
	URL url = new URL(location)
	
	port = (url.getPort() === -1) ? url.getDefaultPort() : url.getPort()
	path = url.getPath()
	query = url.getQuery()
	
	vars.put("redirect-scheme", url.getProtocol())
	vars.put("redirect-host", url.getHost())
	vars.putObject("redirect-port", port)
	vars.put("redirect-path", path) // should be /am/oauth2/realms/root/realms/Citizens/realms/WEB/authorize

	// parse query - split on & then key=value
	def queryParams = url.query?.split('&')
	def mapParams = queryParams.collectEntries { param -> param.split('=').collect { URLDecoder.decode(it) }}

	// these are the main parameters likely to change
	vars.put("redirect_uri", mapParams['redirect_uri'])
	vars.put("client_id", mapParams['client_id'])
	vars.put("state", mapParams['state'])

	if (path.contains("error_description")) {
		AssertionResult.setFailureMessage("***** Path contains error_description");
		AssertionResult.setFailure(true);
	}
} else {
	log.error("***** Expected 302 got {}", prev.getResponseCode())
	AssertionResult.setFailure(true);
}
