<?xml version="1.0" encoding="UTF-8"?>
<jmeterTestPlan version="1.2" properties="5.0" jmeter="5.5">
  <hashTree>
    <TestPlan guiclass="TestPlanGui" testclass="TestPlan" testname="Test Plan" enabled="true">
      <stringProp name="TestPlan.comments"></stringProp>
      <boolProp name="TestPlan.functional_mode">false</boolProp>
      <boolProp name="TestPlan.tearDown_on_shutdown">true</boolProp>
      <boolProp name="TestPlan.serialize_threadgroups">false</boolProp>
      <elementProp name="TestPlan.user_defined_variables" elementType="Arguments" guiclass="ArgumentsPanel" testclass="Arguments" testname="User Defined Variables" enabled="true">
        <collectionProp name="Arguments.arguments"/>
      </elementProp>
      <stringProp name="TestPlan.user_define_classpath"></stringProp>
    </TestPlan>
    <hashTree>
      <ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="HAR to JMeter" enabled="true">
        <stringProp name="ThreadGroup.on_sample_error">continue</stringProp>
        <elementProp name="ThreadGroup.main_controller" elementType="LoopController" guiclass="LoopControlPanel" testclass="LoopController" testname="Loop Controller" enabled="true">
          <boolProp name="LoopController.continue_forever">false</boolProp>
          <stringProp name="LoopController.loops">1</stringProp>
        </elementProp>
        <stringProp name="ThreadGroup.num_threads">1</stringProp>
        <stringProp name="ThreadGroup.ramp_time">1</stringProp>
        <boolProp name="ThreadGroup.scheduler">false</boolProp>
        <stringProp name="ThreadGroup.duration"></stringProp>
        <stringProp name="ThreadGroup.delay"></stringProp>
        <boolProp name="ThreadGroup.same_user_on_next_iteration">true</boolProp>
      </ThreadGroup>
      <hashTree>
        <JSR223Sampler guiclass="TestBeanGUI" testclass="JSR223Sampler" testname="HAR to JMeter" enabled="true">
          <stringProp name="scriptLanguage">groovy</stringProp>
          <stringProp name="parameters">/Users/Shared/Dev/Repos/PerformanceTesting/KBV/KBV-uplift-OK.har</stringProp>
          <stringProp name="filename"></stringProp>
          <stringProp name="cacheKey">true</stringProp>
          <stringProp name="script">/*
Use the &quot;Parameters:&quot; text box for input and output filename(s)

DO NOT USE the &quot;File Name:&quot; text box for the HAR filename

e.g.  Parameters: recorded.har myscript.jmx
or simply give the har filename and a similarly named JMX file will be created
*/

import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovy.xml.MarkupBuilder
import org.apache.jmeter.services.FileServer
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

// include HTTP Header Manager with every Sampler
boolean withHttpHeaders = true

// regex pattern for file extensions to be excluded
String excluded = /(?i).*\.(css|js|jpg|gif|png|doc|pdf|woff2|ico|svg)$/

// use single HTTP Request Defaults
boolean useHttpRequestDefaults = false

// use Cookie Manager - so omit Cookies in Header Manager
boolean useCookieManager = true

/*
&apos;\${__P(@scheme, https)}&apos;
&apos;\${__P(@host, localhost)}&apos;
&apos;\${__P(@port, 443)}&apos;
*/
if (args.length == 2) {
    harFilename = args[0]
    jmxFilename = args[1]
} else if (args.length == 1) {
    harFilename = args[0]
    jmxFilename = args[0].replace(&apos;.har&apos;, &apos;.jmx&apos;)
} else {
    log.error(&apos;***** Must be 1 or 2 parameters. Found {} *****&apos;, args.length)
    log.error(&apos;****************** Exiting *******************&apos;)
    return
}

// resolve ~/ to current base dir if supplied
// otherwise absolute path is used verbatim
// or relative path is relative to the default base dir (where JMeter was started, not necessarily the current base dir)
FileServer fs = FileServer.getFileServer();
harFilepath = fs.resolveBaseRelativeName(harFilename)
jmxFilepath = fs.resolveBaseRelativeName(jmxFilename)

log.info(&apos;************** Converting {} to {}&apos;, harFilepath, jmxFilepath)

try {
    har2JMeter = new Har2JMeter(withHttpHeaders: withHttpHeaders
            , regexExcluded: excluded
            , useHttpRequestDefaults: useHttpRequestDefaults
            , useCookieManager: useCookieManager)
    har2JMeter.convert(new File(harFilepath), new File(jmxFilepath))
} catch (Throwable exception) {
    log.info(exception.message)
}

@Slf4j
class Har2JMeter {

    def withHttpHeaders = true
    def regexExcluded = &apos;&apos;
    def useHttpRequestDefaults = false
    def useCookieManager = false

    // these will be updated when test plan is saved
    def version = &apos;1.2&apos;
    def properties = &apos;5.0&apos;
    def jmeterVersion = &apos;5.4&apos;

   /*
    * Map the JSON from the har file to a JMeterHttpSampler object (defined below)
    * Then construct an XML file from that object 
    */
    def convert(File harFile, File jmxFile) {
        if (!harFile.exists()) {
            log.error(&apos;***** The HAR input file {} does not exist *****&apos;, harFile)
            return
        }

        def jmeterSamplers = []
        def jsonSlurper = new JsonSlurper()
        def har = jsonSlurper.parse(new FileReader(harFile))

        har.log.entries.each { entry -&gt;
            try {
                def request = entry.request
                if (request &amp;&amp; !(request.url ==~ regexExcluded)) {
                    URL url = new URL(request.url)
                    JMeterHttpSampler sampler = new JMeterHttpSampler(url: url, method: request.method)

                    request.headers?.each { header -&gt;
                        sampler.headers.put(header.name, header.value)
                    }

                    request.queryString?.each { param -&gt;
                        sampler.arguments.put(param.name, param.value)
                    }

                    /*
                    JMeter has an idiosyncratic way of handling parameters
                    They&apos;re either query parameters OR body parameters
                    If you need both, list the query parameters explicitly in the path
                    */
                    if (request.postData) {
                        if (request.postData.params) {  // if params array then transfer to arguments
                            log.debug(&quot;***** Adding data parameters array to sampler&quot;)
                            request.postData?.params?.each { param -&gt;
                                sampler.params.put(param.name, param.value)
                                log.debug(&quot;***** Adding body param: {} {}&quot;, param.name, param.value)
                            }
                        }
                        if (request.postData.text) {
                            log.debug(&quot;***** Adding body data to sampler&quot;)
                            sampler.mimeType = request.postData.mimeType
                            sampler.postDataText = request.postData.text
                        }
                    }

                    log.info(&apos;***** Adding {}: {}&apos;, request.method, url.getProtocol() + &quot;://&quot; + url.getAuthority() + url.getPath())
                    jmeterSamplers.add(sampler)

                }
            } catch (MalformedURLException exp) {
                log.warn(&apos;***** The HAR file contains an entry with an invalid URL. This entry will be ignored: {} *****&apos;, entry.request.url)
            }
        }

        jmxFile.text = toJmx(jmeterSamplers)
        log.info(&apos;***** The JMX file {} was successfully created. *****&apos;, jmxFile)
    }

    def toJmx(jmeterSamplers) {
        def writer = new StringWriter()
        def xml = new MarkupBuilder(writer)  // for mapping of object to the XML of the jmx file
        xml.jmeterTestPlan(version: version
                , properties: properties
                , jmeter: jmeterVersion) {

            // Test Plan
            hashTree() {
                TestPlan(guiclass: &apos;TestPlanGui&apos;
                        , testclass: &apos;TestPlan&apos;
                        , testname: &apos;Test Plan&apos;
                        , enabled: true) {
                    stringProp(name: &apos;TestPlan.comments&apos;, &apos;&apos;)
                    boolProp(name: &apos;TestPlan.functional_mode&apos;, false)
                    boolProp(name: &apos;TestPlan.serialize_threadgroups&apos;, false)
                    elementProp(name: &apos;TestPlan.user_defined_variables&apos;
                            , elementType: &apos;Arguments&apos;
                            , guiclass: &apos;ArgumentsPanel&apos;
                            , testclass: &apos;Arguments&apos;
                            , testname: &apos;User Defined Variables&apos;
                            , enabled: true) {
                        collectionProp(name: &apos;Arguments.arguments&apos;)
                    }
                    stringProp(name: &apos;TestPlan.user_define_classpath&apos;)
                }

                // Cookie Manager
                if (useCookieManager) {
                	log.info(&quot;***** Manually add a Cookie Manager for expediency&quot;)
                }

                // User Defined Variables
                hashTree() {
                    Arguments(guiclass: &apos;ArgumentsPanel&apos;
                            , testclass: &apos;Arguments&apos;
                            , testname: &apos;Global Configuration&apos;
                            , enabled: true) {
                        stringProp(name: &apos;TestPlan.comments&apos;, &apos;\nSet @scheme:@host:@port in properties\n&apos;)
                        collectionProp(name: &apos;Arguments.arguments&apos;) {
                            elementProp(name: &quot;scheme&quot;, elementType: &quot;Argument&quot;) {
                                stringProp(name: &apos;Argument.name&apos;, &apos;scheme&apos;)
                                stringProp(name: &apos;Argument.value&apos;, &apos;\${__P(@scheme, https)}&apos;)
                                stringProp(name: &apos;Argument.desc&apos;, &apos;the scheme or protocol&apos;)
                                stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                            }
                            elementProp(name: &quot;host&quot;, elementType: &quot;Argument&quot;) {
                                stringProp(name: &apos;Argument.name&apos;, &apos;host&apos;)
                                stringProp(name: &apos;Argument.value&apos;, &apos;\${__P(@host, localhost)}&apos;)
                                stringProp(name: &apos;Argument.desc&apos;, &apos;the host or server or IP&apos;)
                                stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                            }
                            elementProp(name: &quot;port&quot;, elementType: &quot;Argument&quot;) {
                                stringProp(name: &apos;Argument.name&apos;, &apos;port&apos;)
                                stringProp(name: &apos;Argument.value&apos;, &apos;\${__P(@port, 443)}&apos;)
                                stringProp(name: &apos;Argument.desc&apos;, &apos;the TCP port number&apos;)
                                stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                            }
                        }
                    }

                    // HTTP Request Defaults
                    if (useHttpRequestDefaults) {
                        hashTree()
                        ConfigTestElement(guiclass: &apos;HttpDefaultsGui&apos;
                                , testclass: &apos;ConfigTestElement&apos;
                                , testname: &apos;HTTP Request Defaults&apos;
                                , enabled: true) {
                            elementProp(name: &apos;HTTPsampler.Arguments&apos;
                                    , elementType: &apos;Arguments&apos;
                                    , guiclass: &apos;HTTPArgumentsPanel&apos;
                                    , testclass: &apos;Arguments&apos;
                                    , testname: &apos;HTTP Request Defaults&apos;
                                    , enabled: true) {
                                collectionProp(name: &apos;Arguments.arguments&apos;)
                            }
                            stringProp(name: &apos;HTTPSampler.protocol&apos;, &apos;${scheme}&apos;)
                            stringProp(name: &apos;HTTPSampler.domain&apos;, &apos;${host}&apos;)
                            stringProp(name: &apos;HTTPSampler.port&apos;, &apos;${port}&apos;)
                            stringProp(name: &apos;HTTPSampler.path&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.contentEncoding&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.concurrentPool&apos;, 6)
                            stringProp(name: &apos;HTTPSampler.connect_timeout&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.response_timeout&apos;, &apos;&apos;)
                        }
                        hashTree()
                    }

                }
                // Thread Group
                ThreadGroup(guiclass: &apos;ThreadGroupGui&apos;
                        , testclass: &apos;ThreadGroup&apos;
                        , testname: &apos;Thread Group&apos;
                        , enabled: true) {
                    stringProp(name: &apos;ThreadGroup.on_sample_error&apos;, &apos;continue&apos;)
                    elementProp(name: &apos;ThreadGroup.main_controller&apos;
                            , elementType: &apos;LoopController&apos;
                            , guiclass: &apos;LoopControlPanel&apos;
                            , testclass: &apos;LoopController&apos;
                            , testname: &apos;Loop Controller&apos;
                            , enabled: true) {
                        boolProp(name: &apos;LoopController.continue_forever&apos;, false)
                        stringProp(name: &apos;LoopController.loops&apos;, &apos;1&apos;)
                    }
                    stringProp(name: &apos;ThreadGroup.num_threads&apos;, &apos;1&apos;)
                    stringProp(name: &apos;ThreadGroup.ramp_time&apos;, &apos;1&apos;)
                    longProp(name: &apos;ThreadGroup.start_time&apos;, &apos;1362062247000&apos;)
                    longProp(name: &apos;ThreadGroup.end_time&apos;, &apos;1362062247000&apos;)
                    boolProp(name: &apos;ThreadGroup.scheduler&apos;, false)
                    stringProp(name: &apos;ThreadGroup.duration&apos;, &apos;&apos;)
                    stringProp(name: &apos;ThreadGroup.delay&apos;, &apos;&apos;)
                }

                //Samplers
                hashTree() {
                    jmeterSamplers.each { sampler -&gt;
				    def path = &quot;${sampler.path}&quot;
				    path = path.substring(path.lastIndexOf(&quot;/&quot;))
                        log.debug(&quot;***** sampler path {}&quot;, path)
                        HTTPSamplerProxy(guiclass: &apos;HttpTestSampleGui&apos;
                                , testclass: &apos;HTTPSamplerProxy&apos;
						  , testname: &quot;${sampler.method} ${path}&quot;
//						    Pick one testname
//						  , testname: &quot;${sampler.method} ${sampler.path}&quot;
//						  , testname: &quot;${sampler.path}&quot;
                                , enabled: true) {

                            if ((sampler.mimeType == &apos;application/json&apos;)
                                    || (sampler.mimeType == &apos;application/xml&apos;
                                    || (sampler.mimeType == &apos;application/x-www-form-urlencoded&apos;)
                                    )
                            ) {
                            	  log.debug(&quot;***** Adding postData text from har as Body Data&quot;)
                                boolProp(name: &apos;HTTPSampler.postBodyRaw&apos;, true)
                                elementProp(name: &apos;HTTPsampler.Arguments&apos;
                                        , elementType: &apos;Arguments&apos;) {
                                    collectionProp(name: &apos;Arguments.arguments&apos;) {
                                        elementProp(name: &apos;&apos;, elementType: &apos;HTTPArgument&apos;) {
                                            boolProp(name: &apos;HTTPArgument.always_encode&apos;, true)
                                            stringProp(name: &apos;Argument.value&apos;, sampler.postDataText)
                                            stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                                        }
                                    }
                                }
                            }

                            // This has problems - best use postBodyRaw for all body content 
                            //   only use Parameters for query parameters
                            // if (sampler.mimeType == &apos;application/x-www-form-urlencoded&apos;) {
                            //     log.debug(&quot;***** Adding postData key:value pairs from har as Parameters&quot;)
                            //     elementProp(name: &apos;HTTPsampler.Arguments&apos;
                            //             , elementType: &apos;Arguments&apos;
                            //             , guiclass: &apos;HTTPArgumentsPanel&apos;
                            //             , testclass: &apos;Arguments&apos;
                            //             , testname: &apos;User Defined Variables&apos;
                            //             , enabled: true) {
                            //         collectionProp(name: &apos;Arguments.arguments&apos;) {
                            //             sampler.params.entries().each { param -&gt;
                            //                 elementProp(name: param.key, elementType: &apos;HTTPArgument&apos;) {
                            //                     boolProp(name: &apos;HTTPArgument.always_encode&apos;, false)
                            //                     stringProp(name: &apos;Argument.value&apos;, param.value)
                            //                     stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                            //                     boolProp(name: &apos;HTTPArgument.use_equals&apos;, true)
                            //                     stringProp(name: &apos;Argument.name&apos;, param.key)
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }

                            if (sampler.mimeType) {
                            	  // request has post data so if queryString exists, it must remain as part of path
                            	  if (sampler.url.getQuery() == null) {
                                	stringProp(name: &apos;HTTPSampler.path&apos;, sampler.getPath())
                            	  } else {
                                	stringProp(name: &apos;HTTPSampler.path&apos;, sampler.getPath() + &apos;?&apos; + sampler.url.getQuery())
                            	  }
                            } else {
                                log.debug(&quot;***** Adding queryString name:value pairs from har as Parameters&quot;)
                                stringProp(name: &apos;HTTPSampler.path&apos;, sampler.getPath())
                                elementProp(name: &apos;HTTPsampler.Arguments&apos;
                                        , elementType: &apos;Arguments&apos;
                                        , guiclass: &apos;HTTPArgumentsPanel&apos;
                                        , testclass: &apos;Arguments&apos;
                                        , testname: &apos;User Defined Variables&apos;
                                        , enabled: true) {
                                    collectionProp(name: &apos;Arguments.arguments&apos;) {
                                        sampler.arguments.entries().each { argument -&gt;
                                            elementProp(name: argument.key, elementType: &apos;HTTPArgument&apos;) {
                                                boolProp(name: &apos;HTTPArgument.always_encode&apos;, true)
                                                stringProp(name: &apos;Argument.value&apos;, argument.value)
                                                stringProp(name: &apos;Argument.metadata&apos;, &apos;=&apos;)
                                                boolProp(name: &apos;HTTPArgument.use_equals&apos;, true)
                                                stringProp(name: &apos;Argument.name&apos;, argument.key)
                                            }
                                        }
                                    }
                                }
                            }

                            if (sampler.mimeType == &apos;multipart/form-data&apos;) {
                                boolProp(name: &apos;HTTPSampler.DO_MULTIPART_POST&apos;, true)
                            } else {
                                boolProp(name: &apos;HTTPSampler.DO_MULTIPART_POST&apos;, false)
                            }

                            if (useHttpRequestDefaults) {
                                stringProp(name: &apos;HTTPSampler.protocol&apos;, &apos;&apos;)
                                stringProp(name: &apos;HTTPSampler.domain&apos;, &apos;&apos;)
                                stringProp(name: &apos;HTTPSampler.port&apos;, &apos;&apos;)
                            } else {
                                stringProp(name: &apos;HTTPSampler.protocol&apos;, &apos;https&apos;)
                                stringProp(name: &apos;HTTPSampler.domain&apos;, sampler.domain)
                                stringProp(name: &apos;HTTPSampler.port&apos;, 443)
                            }

                            stringProp(name: &apos;HTTPSampler.connect_timeout&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.response_timeout&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.contentEncoding&apos;, &apos;&apos;)

                            stringProp(name: &apos;HTTPSampler.method&apos;, sampler.method.toUpperCase())
                            boolProp(name: &apos;HTTPSampler.follow_redirects&apos;, false)
                            boolProp(name: &apos;HTTPSampler.auto_redirects&apos;, false)
                            boolProp(name: &apos;HTTPSampler.use_keepalive&apos;, true)
                            boolProp(name: &apos;HTTPSampler.monitor&apos;, false)
                            stringProp(name: &apos;HTTPSampler.embedded_url_re&apos;, &apos;&apos;)
                            stringProp(name: &apos;HTTPSampler.implementation&apos;, &apos;&apos;)
                        }
                        // HTTP Header Manager
                        hashTree() {
                            if (withHttpHeaders &amp;&amp; sampler.headers) {
                                HeaderManager(guiclass: &apos;HeaderPanel&apos;
                                        , testclass: &apos;HeaderManager&apos;
                                        , testname: &apos;HTTP Header Manager&apos;
                                        , enabled: true) {
                                    collectionProp(name: &apos;HeaderManager.headers&apos;) {
                                        sampler.headers.entries().each { header -&gt;
	                                        if (useCookieManager &amp;&amp; (&quot;Cookie&quot;.equalsIgnoreCase(header.key))) {  // omit Cookie
	                                        } else {
	                                        	if ( !(&quot;Connection&quot;.equals(header.key) || &quot;Content-Length&quot;.equals(header.key)))  {  // omit Connection=Keep-Alive and Content-Length
	                                        	   elementProp(name: &apos;&apos;, elementType: &apos;Header&apos;) {
	                                                stringProp(name: &apos;Header.name&apos;, header.key)
	                                                stringProp(name: &apos;Header.value&apos;, header.value)
	                                        	   }
	                                            }
	                                        }
                                        }
                                    }
                                }
                                hashTree()
                            }
                        }
                    }
                }
            }
        }

        return writer.toString()
    }

}

class JMeterHttpSampler {

    URL url
    String method

    String mimeType
    String postDataText

    // need to cater for duplicate keys
    MultiValuedMap&lt;String, String&gt; headers = new ArrayListValuedHashMap&lt;&gt;();  // HTTP headers
    MultiValuedMap&lt;String, String&gt; arguments = new ArrayListValuedHashMap&lt;&gt;();  // from query string
    MultiValuedMap&lt;String, String&gt; params = new ArrayListValuedHashMap&lt;&gt;();  // from post data

    def getPort() { url.port != -1 ? url.port : (&apos;HTTPS&apos;.equalsIgnoreCase(url.protocol) ? 443 : 80) }

    def getDomain() { url.host }

    def getProtocol() { url.protocol }

    def getPath() { url.path }

}
</stringProp>
          <stringProp name="TestPlan.comments">
Use 
  Parameters: 
to select input and/or output filename(s) in base directory 

e.g.  Parameters:    recorded.har myscript.jmx

or simply give the har filename and a similarly named JMX file will be created
</stringProp>
        </JSR223Sampler>
        <hashTree/>
      </hashTree>
    </hashTree>
  </hashTree>
</jmeterTestPlan>
